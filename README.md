#FastFrameWork
    FastFrameWork 快速开发框架是一款基于敏捷并行开发思想和Microsoft .Net构件(插件)开发技术而构建的一个快速开发应用平台。用于帮助中小型软件企业建立一条适合市场快速变化的开发团队，以达到节省开发成本、缩短开发时间，快速适应市场变化的目的。

    FastFrameWork 快速开发框架是适用于小型软件的一套快速开发解决方案。基于DevExpress16.1 开发，采用Ribbon UI设计风格，插件式扩展模块，配合Agile.Net数据访问组件与Agile.Net数据报表组件，能够快速的进行小型软件产品开发。

    Agile.Net数据访问组件是支持多种主流数据库的ORM，[详细介绍](http://www.cnblogs.com/MuNet/p/5740833.html)

    Agile.Net数据报表组件提供报表打印、报表预览、报表设计、自定义报表数据源等二次开发功能，[详细介绍](http://www.cnblogs.com/MuNet/p/5547085.html)
    欢迎大家参与FastFrameWork 快速开发框架共同维护，欢迎大家提意见，使用过程中如果有疑问或者想参与框架代码维护，请加入Agile.Net QQ交流群(10062867)