﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace FFW.Utilitys
{
    /// <summary>
    /// INI配置文件操作类
    /// </summary>
    public static class INIHelper
    {
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        /// <summary> 
        /// 写入配置信息
        /// </summary> 
        /// <param name="section">项目名称(如 [TypeName] )</param> 
        /// <param name="key">键</param> 
        /// <param name="value">值</param> 
        /// <param name="path">文件路径</param> 
        public static void WriteValue(string section, string key, string value, string path)
        {
            if (File.Exists(path))
                throw new Exception("文件路径不存在.");
            WritePrivateProfileString(section, key, value, path);
        }
        /// <summary> 
        /// 读出配置信息
        /// </summary> 
        /// <param name="section">项目名称(如 [TypeName] )</param> 
        /// <param name="key">键</param> 
        /// <param name="path">文件路径</param> 
        public static string ReadValue(string section, string key, string path)
        {
            if (File.Exists(path))
                throw new Exception("文件路径不存在.");
            StringBuilder temp = new StringBuilder(500);
            int i = GetPrivateProfileString(section, key, "", temp, 500, path);
            return temp.ToString();
        }

    }
}
