﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraWaitForm;
using DevExpress.XtraSplashScreen;

namespace FFW.Utilitys
{
    internal partial class WaitFormEx : WaitForm
    {
        public WaitFormEx()
        {
            InitializeComponent();

        }

        #region Overrides

        public override void SetCaption(string caption)
        {
            base.SetCaption(caption);
            this.progressPanel1.Caption = caption;
        }
        public override void SetDescription(string description)
        {
            base.SetDescription(description);
            this.progressPanel1.Description = description;
        }
        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }

        #endregion

        public enum WaitFormCommand
        {
        }

        private void WaitControlForm_Load(object sender, EventArgs e)
        {

        }
    }


    /// <summary>
    /// 异步等待控件
    /// </summary>
    public class WaitControlEx : IDisposable
    {

        /// <summary>
        /// 弹出等待窗体
        /// </summary>
        public static void Show()
        {
            if (SplashScreenManager.Default == null)
                SplashScreenManager.ShowForm(typeof(WaitFormEx), true, true);
        }
        
        /// <summary>
        /// 弹出等待窗体
        /// </summary>
        /// <param name="description">窗体显示文本</param>
        public static void Show(string description)
        {
            if (SplashScreenManager.Default == null)
                SplashScreenManager.ShowForm(typeof(WaitFormEx), true, true);
            SetDescription(description);
        }     
      
        /// <summary>
        /// 关闭等待窗体
        /// </summary>
        public static void Close()
        {
            if (SplashScreenManager.Default != null)
                SplashScreenManager.CloseDefaultWaitForm();
        }
        /// <summary>
        /// 设置等待窗体显示文本
        /// </summary>
        /// <param name="description">窗体显示文本</param>
        public static void SetDescription(string description)
        {
            if (SplashScreenManager.Default == null)
                return;
            SplashScreenManager.Default.SetWaitFormDescription(description);
        }

      
        /// <summary>
        /// 等待窗体是否已启动？
        /// </summary>
        /// <returns></returns>
        public static bool IsShow
        {
            get
            {
                if (SplashScreenManager.Default == null)
                    return false;
                return true;
            }
        }
        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose()
        {
            if (SplashScreenManager.Default != null)
                SplashScreenManager.CloseDefaultWaitForm();
            this.Dispose();
        }
    }

}